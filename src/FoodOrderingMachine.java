import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton CurryButton;
    private JButton ramenButton;
    private JButton SpaghettiButton;
    private JButton PizzaButton;
    private JButton CakeButton;
    private JButton SteakButton;
    private JButton checkButton;
    private JTextArea textArea1;
    private JLabel total;
    private JButton cancelButton;
    int sum = 0;


    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0) {
            JOptionPane.showMessageDialog(null, "Order for "+food+" received");

            String currentText = textArea1.getText();
            textArea1.setText(currentText + food+ "      " + price + " yen" + "\n");

            sum += price ;
            total.setText("Total    "+ sum + "yen");

        }
    }
    void order2(String food,int price) {
        int confirmation2 = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation2 == 0) {
            int confirmation3 = JOptionPane.showConfirmDialog(null,
                    "Would you like a large portion ?",
            "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation3 == 1) {
                JOptionPane.showMessageDialog(null, "Order for " + food + " received");

                String currentText = textArea1.getText();
                textArea1.setText(currentText + food + "      " + price + " yen" + "\n");

                sum += price;
                total.setText("Total    " + sum + "yen");
            }
            if (confirmation3 == 0) {
                JOptionPane.showMessageDialog(null, "Order for " + food + "(large portion) received");
                int price2 = price+50;
                String currentText = textArea1.getText();
                textArea1.setText(currentText + food + "(large portion)      " + price2 + " yen" + "\n");

                sum += price2;
                total.setText("Total    " + sum + "yen");
            }


        }
    }

    public FoodOrderingMachine() {
        CurryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Curry and rice",400);

            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Ramen",600);
            }
        });
        SpaghettiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Spaghetti",500);
            }
        });
        SteakButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Steak",700);
            }
        });
        PizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pizza",300);
            }
        });
        CakeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cake",200);
            }
        });
        checkButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to check out ?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {

                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+ sum + " yen.");
                    textArea1.setText("");
                    total.setText("");
                    sum = 0;

                }


            }

        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to cancel ?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    textArea1.setText("");
                    total.setText("");
                    sum = 0;
                }

            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
